import os
import sys
import importlib.util
from flask import Flask, request
import click
import yaml
import base64
import random
from datetime import datetime


class MockContext:
    """Mock Context Object (mimicing google.cloud.functions_v1.context.Context)"""

    def __init__(self):
        # random 15-digit int
        self.event_id = random.randint(100000000000000, 999999999999999)
        # UTC timestamp, ISO-formatted, using 'Z' instead of '+00:00'
        self.timestamp = '{}Z'.format(datetime.utcnow().isoformat(timespec='milliseconds'))
        self.event_type = 'google.pubsub.topic.publish'
        self.resource = {
            'service': 'pubsub.googleapis.com',
            'name': 'projects/cloud-functions-python-emulator/topics/mock_topic',
            'type': 'type.googleapis.com/google.pubsub.v1.PubsubMessage'
        }

    def __str__(self):
        return str(self.__dict__)


@click.command()
@click.option('--module', '-m', 'module_path', required=True, help='path to the module containing the function')
@click.option('--function', '-f', required=True, help='name of the function as you wrote in the code')
@click.option('--endpoint', '-e', help='name of the endpoint to use if differente from the function name')
@click.option('--port', '-p', default=5000, help='port number to use in the server')
@click.option('--env-vars-file', '-v', help='YAML file with definitions for environment variables')
@click.option('--pubsub-message', help='simulate trigger from PubSub with this message')
def main(module_path, function, endpoint, port, env_vars_file, pubsub_message):
    """ Process command line and load cloud function """
    # emulate function name env vars
    os.environ.update({
        'FUNCTION_NAME': endpoint if endpoint is not None else function,
        'X_GOOGLE_FUNCTION_NAME': endpoint if endpoint is not None else function
    })

    # set custom env vars if specified
    if env_vars_file is not None:
        try:
            with open(env_vars_file) as f:
                os.environ.update(yaml.safe_load(f))
        except Exception as e:
            sys.exit("ERROR Failed to process env vars file {}: {}".format(env_vars_file, e))

    # if no endpoint was provided, we use the function name instead
    if endpoint is None:
        endpoint = function

    # add the function's dirname to sys.path so the function can import properly
    module_dir = os.path.dirname(os.path.abspath(module_path))
    sys.path.append(module_dir)

    # dinamically import the module and the function to run
    spec = importlib.util.spec_from_file_location('cloud_function', module_path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    fn = getattr(module, function)

    # simulate pubsub trigger if specified
    if pubsub_message is not None:
        trigger_pubsub(pubsub_message, fn)
    # otherwise serve HTTP function
    else:
        serve(module_path, function, endpoint, port, fn)


def trigger_pubsub(pubsub_message, fn):
    """Trigger function via pubsub message publish"""
    # base64 encode pubsub_message

    # create mock context object
    context = MockContext()

    # create mock data dict
    data = {
        '@type': 'type.googleapis.com/google.pubsub.v1.PubsubMessage',
        'attributes': None,
        'data': base64.b64encode(pubsub_message.encode()).decode()
    }

    # call function
    fn(data, context)


def serve(module_path, function, endpoint, port, fn):
    """ Serve functions using Flask """
    # serve the code in the specified route
    print(' * Serving ' + function + '@' + module_path + ' in http://localhost:' + str(port) + '/' + endpoint)
    app = Flask('gcpfemu')
    @app.route('/' + endpoint)
    def index():
        return fn(request)
    app.run(port=port, debug=True)


if __name__ == '__main__':
    main()
